### The iF|City Website

## Dependencies

nodejs: https://nodejs.org/en/download/

Angular v. 9.1.3: https://angular.io/ (npm i -g @angular/cli@9.1.3)

Git: https://git-scm.com/downloads

## Recommended

Visual Code: https://code.visualstudio.com/   (TSLint)

cmder: https://cmder.net/

## SetUp

1. install all dependencies
2. clone the git project
3. type npm i inside the project folder
3. (optional) if you get the ExecutionPolicy error, open the powershell as administrator and enter "Set-ExecutionPolicy Unrestricted -Scope CurrentUser"
4. run "ng serve"

### Set up git
Hit following commands into the terminal

1. git init (only if not used git clone)
2. git config --global user.name "[your git username]"
3. git config --global user.email "[your git email]"
4. git remote add origin https://gitlab.com/Kn0t/if-city-website.git  (only if not used git clone)

## Workflow

### In order to work in a team and contribute your work, this is important!

1. assign to an Issue
2. create a branch equal named to the issue
3. do your work
4. commit and push
5. create a merge request from your branch to the 'frontend' branch
6. switch to the frontend branch!

if everything is fine, it will be merged in to the dev branch.

## how does good work look?

### Use the issue board!

1. strictly only work on the issue you assinged yourself to
2. try to use our design pattern (you can find global styles in the theme.scss or use angular material)
3. write clean code!
4. try to avoid complex functions (It may happen that to complex code won't be accepted)
5. noticed some missing dependencies? Put the issue back to 'Open', write a comment about what is missing, and contact UrinStone
