import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { SidenavService } from './service/sidenav.service';
import { MatDrawer } from '@angular/material/sidenav';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild('sidenav') public sidenav: MatDrawer;

  constructor(private sidenavService: SidenavService, public auth: AuthService) {}

  ngOnInit() {
    this.auth.authenticate();
  }

  ngAfterViewInit() {
    this.sidenavService.setSidenav(this.sidenav);
    
  }

  
}
