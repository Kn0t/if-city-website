import { Directive, Input, ElementRef, AfterViewInit, Renderer2 } from '@angular/core';
import { DbService } from '../service/db.service';
import { Skin } from '../entities/skin';
import { Observable } from 'rxjs';



@Directive({
  selector: '[skin]'
})
export class SkinDirective implements AfterViewInit {
  @Input('name') skin: string | Observable<any>
  @Input('scale') scale: number = 1;
  @Input('center') center:boolean = false;

  constructor(private el: ElementRef, private db: DbService, private renderer: Renderer2) { }

  ngAfterViewInit() {
    if (this.skin === undefined) this.skin = 'default';

    if (typeof(this.skin) === 'string') {
      this.db.getSkin(this.skin as string).subscribe(res => {
        const skinData = res as any;
        const skin = new Skin(this.skin as string, skinData.img.img.data.data, this.scale);

        skin.img.subscribe(image => {
          if (image !== undefined) {
            this.el.nativeElement.parentElement.replaceChild(
              image,
              this.el.nativeElement
            );
          }
        });
      });
    } else if (this.skin instanceof Observable) {
      this.skin.subscribe(user => {
        const uSkin = user.skin ? user.skin : user.web.skin;
        this.db.getSkin(uSkin).subscribe(res => {
          const skinData = res as any;
          const skin = new Skin(user.skin, skinData.img.img.data.data, this.scale);

          skin.img.subscribe(image => {
            if (image !== undefined) {
              this.el.nativeElement.parentElement.replaceChild(
                image,
                this.el.nativeElement
              );
            }
          });
        });
      });
    }
  }
}



