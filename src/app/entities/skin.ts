import { BehaviorSubject } from 'rxjs';


export class Skin {
    name: string;
    img = new BehaviorSubject<any>(undefined);
    scale: number;

    constructor(name: string, img: string[], scale: number = 1) {
        this.name = name;
        this.scale = scale;
        this.initImg(img);
    }

    async initImg(img) {
        this.img.next(await this.render(img));
    }

    private async render(skin) {
        skin = this.arrayBufferToBase64(skin);
        const canvas = document.createElement('canvas');
        canvas.width = 96 * this.scale;
        canvas.height = 64 * this.scale;

        const ctx = canvas.getContext('2d');

        const image = new Image();

        image.onload = () => {
            ctx.drawImage(image, 192, 64, 64, 32, 10 * this.scale, 33 * this.scale, 60 * this.scale, 30 * this.scale);  // back feet shadow
            ctx.drawImage(image, 192, 32, 64, 32, 8 * this.scale, 32 * this.scale, 64 * this.scale, 32 * this.scale);   // back feet
            ctx.drawImage(image, 96.5, 0, 96, 96, 16 * this.scale, 0 * this.scale, 64 * this.scale, 64 * this.scale);   // body shadow
            ctx.drawImage(image, 0, 0, 96, 96, 16 * this.scale, 0 * this.scale, 64 * this.scale, 64 * this.scale);      // body
            ctx.drawImage(image, 192, 64, 64, 32, 26 * this.scale, 33 * this.scale, 60 * this.scale, 30 * this.scale);  // front feet shadow
            ctx.drawImage(image, 192, 32, 64, 32, 24 * this.scale, 32 * this.scale, 64 * this.scale, 32 * this.scale);  // front feet
            ctx.drawImage(image, 64, 96, 32, 32, 36.5 * this.scale, 13 * this.scale, 26 * this.scale, 25 * this.scale); // left eye
            ctx.drawImage(image, 64, 96, 32, 32, 45 * this.scale, 13 * this.scale, 26 * this.scale, 25 * this.scale);   // right eye
            ctx.scale(-1, 1);
            ctx.save();
            ctx.restore();
        };

        image.src = 'data:image/png;base64,' + skin;
        return canvas;
    }

    arrayBufferToBase64(buffer) {
        let binary = '';
        const bytes = [].slice.call(new Uint8Array(buffer));
        bytes.forEach((b) => binary += String.fromCharCode(b));
        return window.btoa(binary);
    }

    // render2(skin) {
  //   const canvas = document.createElement('canvas');
  //   canvas.width = 256;
  //   canvas.height = 128;

  //   const ctx = canvas.getContext('2d');
  //   const pixelData = ctx.getImageData(0, 0, canvas.width, canvas.height);

  //   // body
  //   for (let y = 13; y < 88; y++) {
  //     for (let x = 0; x < 100; x++) {
  //       const index = (x + y * canvas.width) * 4;

  //       pixelData.data[index + 0] = pixelData.data[index + 0] - pixelData.data[index + 0] * 0.6 + bodyColor[0];
  //       pixelData.data[index + 1] = pixelData.data[index + 1] - pixelData.data[index + 1] * 0.6 + bodyColor[1] * 0.68;
  //       pixelData.data[index + 2] = pixelData.data[index + 2] - pixelData.data[index + 2] * 0.6 + 0 bodyColor[2] 0.81;
  //     }
  //   }

  //   // foot1
  //   for (let y = 8; y < 38; y++) {
  //     for (let x = 190; x < 255; x++) {
  //       const index = (x + y * canvas.width) * 4;

  //       pixelData.data[index + 0] = pixelData.data[index + 0] - pixelData.data[index + 0] * 0.6; // replace 0 with body color
  //       pixelData.data[index + 1] = pixelData.data[index + 1] - pixelData.data[index + 1] * 0.6 + 0 * 0.68;
  //       pixelData.data[index + 2] = pixelData.data[index + 2] - pixelData.data[index + 2] * 0.6 + 0 * 0.81;
  //     }
  //   }

  //   // foot2
  //   for (let y = 33; y < 58; y++) {
  //     for (let x = 210; x < 255; x++) {
  //       const index = (x + y * canvas.width) * 4;

  //       pixelData.data[index + 0] = pixelData.data[index + 0] - pixelData.data[index + 0] * 0.6; // replace 0 with body color
  //       pixelData.data[index + 1] = pixelData.data[index + 1] - pixelData.data[index + 1] * 0.6 + 0 * 0.68;
  //       pixelData.data[index + 2] = pixelData.data[index + 2] - pixelData.data[index + 2] * 0.6 + 0 * 0.81;
  //     }
  //   }

  //   ctx.putImageData(pixelData, 0, 0);

  //   return canvas;
  // }
}
