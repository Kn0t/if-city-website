export interface Tag {
    name: string;
    desc: string;
    color: string;
}
