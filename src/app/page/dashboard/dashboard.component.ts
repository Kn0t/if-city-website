import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import * as CanvasJS from '../../../assets/canvasjs.min';
import { DbService } from 'src/app/service/db.service';
import { AppService } from 'src/app/service/app.service';

import { ResizedEvent } from 'angular-resize-event';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  @ViewChild('container', {static: false}) container: ElementRef;

  constructor(public auth: AuthService, private db: DbService, public app: AppService) { }
  img: any;
  user: any;
  upgrades = new Map<any, any>();

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.renderCharts();
    this.auth.isLogged.subscribe(res => {
      for (const upgradeElement in this.auth.user) {
        if (upgradeElement !== 'general' && upgradeElement !== 'web') {
          this.upgrades.set(upgradeElement, this.auth.user[upgradeElement]); // Make this async to remove error
        }
      }
    });
  }

  onResize(event: ResizedEvent) {
    if (event.oldWidth !== event.newWidth) {
      this.auth.isLogged.subscribe(res => {
        if (this.auth.user) this.renderCharts();
      });
    }
  }

  renderCharts() {
    CanvasJS.addColorSet('levelColors', ['#7D3C98', '#2E86C1']);
    CanvasJS.addColorSet('kdColors', ['#C0392B', '#AF7AC5']);
    this.renderLevelChart();
    this.renderMoneyChart();
    this.renderKDChart();
  }

  renderLevelChart() {
    const neededExp = this.auth.user.general.neededExp - this.auth.user.general.experience;
    const chart = new CanvasJS.Chart('level-chart', {
      animationEnabled: false,
      colorSet: 'levelColors',
      toolTip: {
        content: '{label}: {y}'
      },
      data: [{
        type: 'doughnut',
        startAngle: 270,
        indexLabel: ' ',
        indexLabelLineThickness: 0,
        explodeOnClick: false,
        dataPoints: [
          { y: this.auth.user.general.experience, label: 'Experience'},
          { y: neededExp, label: 'Needed Experience'}
        ]
      }]
    });
    chart.render();
  }

  renderKDChart() {
    const chart = new CanvasJS.Chart('kd-chart', {
      animationEnabled: false,
      colorSet: 'kdColors',
      toolTip: {
        content: '{label}: {y}'
      },
      data: [{
        type: 'doughnut',
        startAngle: 270,
        indexLabel: ' ',
        indexLabelLineThickness: 0,
        explodeOnClick: false,
        dataPoints: [
          { y: this.auth.user.general.kills, label: 'Kills'},
          { y: this.auth.user.general.deaths, label: 'Deaths'}
        ]
      }]
    });
    chart.render();
  }

  renderMoneyChart() {
    const chart = new CanvasJS.Chart('money-chart', {
      colorSet: 'moneyColors',
      axisY: {
        gridThickness: 0,
        stripLines: [
          {
            value: 0,
            showOnTop: true,
            color: 'gray',
            thickness: 2
          }
        ]
      },
      axisX: {
        title: '',
        minimum: 0,
        maximum: this.auth.user.web.moneyBankLogs.length - 1,
        tickLength: 0,
        lineThickness: 0,
        margin: 0,
        valueFormatString: ' ',
      },
      data: [{
        type: 'splineArea',
        color: '#009AD5dd',
        markerSize: 5,
        yValueFormatString: '#,###$',
        dataPoints: this.auth.user.web.moneyBankLogs.map((data, index) => {
          return {y: data, label: this.auth.user.web.moneyBankLogs.length - index};
        })
      },
      {
        type: 'splineArea',
        color: '#00B50066',
        markerSize: 5,
        yValueFormatString: '#,###$',
        dataPoints: this.auth.user.web.moneyPocketLogs.map((data, index) => {
          return {y: data, label: index};
        })
      }]
    });
    chart.render();
  }

  toChartData(data: object | any[]) {
    if (data instanceof Object) {
      return Object.keys(data).map(key => [key, data[key]]);
    }
  }
}
