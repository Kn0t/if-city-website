import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AuthService } from '../../service/auth.service';
import { AppService } from 'src/app/service/app.service';
import { DbService } from 'src/app/service/db.service';
import { LoginComponent } from '../login/login.component';
import { SidenavService } from 'src/app/service/sidenav.service';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MapOperator } from 'rxjs/internal/operators/map';

interface User {
  name: string;
  playerName: string;
  skin: string;
}

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  servers = [
    '31.186.251.91:8303',
    '31.186.251.91:8304'
  ];
  loginName: string;
  loginPass: string;
  user: any;
  changelogs: Observable<any>;
  admins: Observable<any[]>;
  mods: Observable<any[]>;
  webmasters: Observable<any[]>;
  mappers: Observable<any[]>;
  police: Observable<any[]>;
  top5: Observable<User[]>;

  constructor(
    public auth: AuthService,
    public app: AppService,
    private db: DbService,
    public dialog: MatDialog,
    public sidenav: SidenavService) { }
    observable: Observable<number>;

  ngOnInit(): void {
    this.user = this.auth.user;
    this.initChangeLogs();
    this.initTeam();
    this.initTop5();
  }

  initTop5() {
    this.top5 = this.db.getRanking(1).pipe(map(val => val.slice(0, 5)));
  }

  initChangeLogs() {
    this.changelogs = this.db.getNewestChangeLogs();
  }

  initTeam() {
    this.admins = this.db.getAdmins();
    this.mods = this.db.getMods();
    this.webmasters = this.db.getWebmasters();
    this.mappers = this.db.getMappers();
    this.police = this.db.getPolice();
  }
}
