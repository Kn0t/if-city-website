import { Component, OnInit, Inject, OnDestroy, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { AppComponent } from '../../app.component';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app.service';

interface DialogData {
  name: string;
  pass: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('err', {static: false}) errWrongPw: ElementRef;

  err = false;

  userForm = new FormControl('', [
    Validators.required,
  ]);

  passForm = new FormControl('', [
    Validators.required,
  ]);

  constructor(
    private app: AppService,
    private renderer: Renderer2,
    public dialogRef: MatDialogRef<AppComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.isLogged.subscribe(res => {
      if (!res && (this.userForm.valid && this.passForm.valid)) {
        console.log('wrong user pass')
        this.displayErr();
      } else if (res && (this.userForm.valid && this.passForm.valid)) {
        this.dialogRef.close();
      }
    });
  }

  ngOnDestroy() {
  }

  login() {
    if (this.userForm.valid && this.passForm.valid) {
      this.auth.login(this.data.name, this.data.pass);
    }
  }

  async displayErr() {
    this.err = true;
    this.renderer.addClass(this.errWrongPw.nativeElement, 'err-drop');
  }

}
