import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { ActivatedRoute } from '@angular/router';
import { DbService } from 'src/app/service/db.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;

  constructor(
    public auth: AuthService,
    private route: ActivatedRoute,
    public db: DbService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.db.getUser(this.route.snapshot.paramMap.get('username')).subscribe(res => {
        this.user = res[0];
      });
    });
  }
}
