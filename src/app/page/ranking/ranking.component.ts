import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DbService } from 'src/app/service/db.service';
import { Observable } from 'rxjs';

interface User {
  name: string;
  playerName: string;
  skin: string;
}

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = ['rank', 'user'];
  dataSource = new MatTableDataSource<User>();

  constructor(private db: DbService) { }

  ngOnInit(): void {
    this.setUsers(1);
    this.dataSource.paginator = this.paginator;
  }

  setUsers(startId: number) {
    this.db.getRanking(startId).subscribe(res => this.dataSource.data = res);
  }
}
