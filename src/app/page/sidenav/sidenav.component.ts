import { Component, OnInit, Renderer2, ElementRef, ViewChild, HostListener, AfterViewInit } from '@angular/core';
import { SidenavService } from 'src/app/service/sidenav.service';
import { AuthService } from 'src/app/service/auth.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit, AfterViewInit {
  @ViewChild('container', {static: false}) container: ElementRef;

  isLanding: boolean = false;

  @HostListener('window:scroll', ['$event']) onWindowScroll(event) {
    if (this.isLanding) {
      if (window.scrollY < 400) {
        this.renderer.removeClass(this.container.nativeElement, 'top');
      } else {
        this.renderer.addClass(this.container.nativeElement, 'top');
      }
    }
  }

  constructor(
    private service: SidenavService,
    private auth: AuthService,
    private renderer: Renderer2,
    private router: Router) { }

  ngOnInit(): void {
    this.auth.isLogged.subscribe(res => {
      if (res) {
        if (this.auth.user) this.service.open();
      } else {
        this.service.close();
      }
    });
  }

  ngAfterViewInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/') {
          this.isLanding = true;
          if (window.scrollY < 400) {
            this.renderer.removeClass(this.container.nativeElement, 'top');
          } else {
            this.renderer.addClass(this.container.nativeElement, 'top');
          }
        } else {
          this.isLanding = false;
          this.renderer.removeClass(this.container.nativeElement, 'top');
        }
      }
    });
  }
}


