import { Component, OnInit, Input, AfterViewInit, Renderer2, ViewChild, ElementRef, HostListener } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AuthService } from 'src/app/service/auth.service';
import { SidenavService } from 'src/app/service/sidenav.service';
import { LoginComponent } from '../login/login.component';
import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs';
import { DbService } from 'src/app/service/db.service';
import { EventEmitter } from 'protractor';
import { AppService } from 'src/app/service/app.service';
import { app } from 'server';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit, AfterViewInit{
  @ViewChild('topbar', {static: false}) topbar: ElementRef;
  @ViewChild('searchInputEl', {static: false}) elSearchInput: ElementRef;
  @ViewChild('searchbar', {static: false}) elSearchBar: ElementRef;

  @Input() hasColor: boolean = true;
  @Input() fixed: boolean = false;
  @Input() placeholder: boolean = false;

  dropdownFlag = true;
  isLanding = false;
  loginName: string;
  loginPass: string;
  logged: boolean;
  user: any;
  searchBarStatus: boolean = false;
  searchInput: string;
  searchRes: any[];

  @HostListener('window:scroll', ['$event']) onWindowScroll(event) {
    if (this.fixed && this.isLanding) {
      this.toggleAnim();
    }
  }

  @HostListener('window:resize', ['$event']) onWindowResize(event) {
    if (this.fixed && this.isLanding) {
      this.toggleAnim();
    }
  }

  constructor(
    private appSevice: AppService,
    public dialog: MatDialog,
    public auth: AuthService,
    public sidenavService: SidenavService,
    private renderer: Renderer2,
    private router: Router,
    private db: DbService) {}

  ngOnInit() {
    this.auth.isLogged.subscribe(res => {
      if (res) {
        this.user = this.auth.user;
      }
      this.logged = res;
    });

    this.renderer.listen('window', 'click', (e: Event) => {
      const elements = this.appSevice.allElChildrenToArray(this.elSearchBar.nativeElement);
      elements.push(this.elSearchBar.nativeElement);

      if (elements.find(val => val === e.target) === undefined) {
        this.searchBarStatus = false;
      }
    });
  }

  ngAfterViewInit() {
    this.initStyling();
  }

  initStyling() {
    if (this.hasColor) {
      this.renderer.addClass(this.topbar.nativeElement, 'color');
    }

    if (this.fixed) {
      this.renderer.addClass(this.topbar.nativeElement, 'fixedHeader');
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/') {
          this.renderer.setStyle(this.topbar.nativeElement, 'top', '-64px');
          this.isLanding = true;
          this.toggleAnim();
        } else {
          this.renderer.setStyle(this.topbar.nativeElement, 'top', '0');
          this.isLanding = false;
          this.renderer.removeClass(this.topbar.nativeElement, 'slide-bottom');
          this.renderer.removeClass(this.topbar.nativeElement, 'slide-top');
        }
      }
    });
  }

  toggleAnim() {
    if (window.scrollY >= 400 && this.dropdownFlag) {
      this.renderer.removeClass(this.topbar.nativeElement, 'slide-top');
      this.renderer.addClass(this.topbar.nativeElement, 'slide-bottom');
      this.dropdownFlag = false;
    }
    if (window.scrollY < 400 && !this.dropdownFlag) {
      this.renderer.removeClass(this.topbar.nativeElement, 'slide-bottom');
      this.renderer.addClass(this.topbar.nativeElement, 'slide-top');
      this.dropdownFlag = true;
    }
  }

  openLogin() {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '500px',
      data: {name: this.loginName, pass: this.loginPass }
    });
  }

  logout() {
    this.auth.logout();
  }

  search() {
    if (this.searchInput && this.searchBarStatus) {
      this.db.getUser(this.searchInput).subscribe(res => this.searchRes = res);
    } else {
      this.searchBarStatus = !this.searchBarStatus;
      setTimeout(() => { // this will make the execution after the above boolean has changed
        this.elSearchInput.nativeElement.focus();
      }, 0);
    }
  }
}
