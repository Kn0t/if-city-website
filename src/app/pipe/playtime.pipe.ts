import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'playtime'
})
export class PlaytimePipe implements PipeTransform {

  transform(val: number, args?: any) {
    const min = Math.floor((val / 60) % 60);
    const hours = Math.floor((val / 3600) % 24);
    const days = Math.floor(val / 60 / 60 / 24);

    const res = days > 0 ? `${days}D ${hours}H ${min}M` : hours > 0 ? `${hours}H ${min}M` : `${min}M`;
    return res;
  }

}
