import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortNumber'
})
export class ShortNumberPipe implements PipeTransform {

  transform(val: number, args?: any): any {
    if (isNaN(val)) return null;
    if (val === null) return null;
    if (val === 0) return null;

    let abs = Math.abs(val);
    const rounder = Math.pow(10, 1);
    const isNeg = val < 0;
    let key = '';

    const powers = [
      {key: 'Q', value: Math.pow(10, 15)},
      {key: 'T', value: Math.pow(10, 12)},
      {key: 'B', value: Math.pow(10, 9)},
      {key: 'M', value: Math.pow(10, 6)},
      {key: 'K', value: Math.pow(10, 3)},
    ];

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < powers.length; i++) {
      let reduced = abs / powers[i].value;
      reduced = Math.round(reduced * rounder) / rounder;

      if (reduced >= 1) {
        abs = reduced;
        key = powers[i].key;
        break;
      }
    }

    return (isNeg ? '-' : '') + abs + key;
  }

}
