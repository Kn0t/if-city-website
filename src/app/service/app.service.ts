import { Injectable, ElementRef } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor() { }

  copy(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  public async delay(ms: number) {
    return await new Promise( resolve => setTimeout(resolve, ms) );
  }

  public scrollToElement($element, yOffset = 0): void {
    const y = $element.getBoundingClientRect().top + window.pageYOffset + yOffset;

    window.scrollTo({top: y, behavior: 'smooth'});
  }

  public allElChildrenToArray(el): any[] {
    let elements = Array.from(el.children) as any[];

    elements.forEach(e => {
      const childs = Array.from(e.children) as any[];
      if (childs.length > 0) elements = elements.concat(this.allElChildrenToArray(e));
    });

    return elements;
  }
}
