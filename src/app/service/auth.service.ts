import { Injectable, AfterViewInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, BehaviorSubject, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private server = 'http://68.183.77.132:3000';
  user: any;
  isLogged = new BehaviorSubject<boolean>(true);

  constructor(
    private http: HttpClient,
    private jwt: JwtHelperService) {}

  login(userName: string, pass: string) {
    this.http.post<any>(
      this.server + '/user/login',
      {
        name: userName,
        password: pass
      },
      {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
        })
      }
    ).subscribe((res: any) => {
      localStorage.setItem('access_key', res.accessToken);
      this.user = res.userData;
      this.isLogged.next(true);
      delete res.accessToken;
    }, err => {
      this.isLogged.next(false);
    });
  }

  authenticate() {
    const token = localStorage.getItem('access_key');

    if (!token) {
      this.isLogged.next(false);
      return;
    }
    if (!this.jwt.isTokenExpired(token)) {
      this.http.post<any>(
        this.server + '/user/login',
        {},
        {
          headers: new HttpHeaders({
            'Authorization': `Barear ${token}`
          })
        }
      ).subscribe((res: any) => {
        localStorage.setItem('access_key', res.accessToken);
        this.user = res.userData;
        this.isLogged.next(true);
        delete res.accessToken;
      }, err => {
        this.isLogged.next(false);
      });
    } else {
      this.isLogged.next(false);
    }
  }

  logout() {
    this.user = undefined;
    this.isLogged.next(false);
    localStorage.removeItem('access_key');
  }
}
