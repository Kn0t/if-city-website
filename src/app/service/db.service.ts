import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Tag } from '../entities/tag';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  private server = 'http://68.183.77.132:3000';

  constructor(private http: HttpClient, private auth: AuthService) { }

  getUser(searchParam: string): Observable<any[]> {
    return this.http.get<any>(
      `${this.server}/user/${this.buildQuery('_id', [searchParam])}`,
      {
      headers: new HttpHeaders({
        'Authorization': `Barear ${localStorage.getItem('access_token')}`
      })
    });
  }

  getChangeLogs() {
    const req = this.http.get<any>(
      this.server + '/changelogs',
      {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        })
    });

    req.subscribe(res => {
      return res;
    });
  }

  getNewestChangeLogs(): Observable<any[]> {
    return this.http.get<any[]>(
      this.server + '/changelogs/newest',
      {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        })
    });
  }

  getAdmins(): Observable<any> {
    return this.http.get<any>(`${this.server}/user/?role=admin`);
  }

  getMods() {
    return this.http.get<any>(`${this.server}/user/?role=moderator`);
  }

  getWebmasters() {
    return this.http.get<any>(`${this.server}/user/?role=webmaster`);
  }

  getMappers() {
    return this.http.get<any>(`${this.server}/user/?role=mapper`);
  }

  getPolice() {
    return this.http.get<any>(`${this.server}/user/?role=police`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getSkin(name: string) {
    return this.http.get(
      this.server + '/skin/' + name,
      {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        })
      });
  }

  getRanking(startId: number): Observable<any[]> {
    return this.http.get<any>(
      this.server + '/rank/' + startId,
      {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        })
      }).pipe(map(res => res.ranking))
  }

  getTags(ids: string[]) {
    return this.http.get<Tag[]>(
      `${this.server}/tag${this.buildQuery('_id', ids)}`,
      {
        headers: new HttpHeaders({
          'Authorization': `Bearer ${localStorage.getItem('access_token')}`
        })
      });
  }

  buildQuery(name: string, val: string[]): string {
    let query = '?';
    for (let param of val) {
      query += `${name}=${param}&`;
    }
    return query;
  }
}
